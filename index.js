const express = require("express");
const FB = require("facebook-node");
const bodyParser = require("body-parser");

var app = express();
var port = process.env.PORT || 8080;

app.set('view engine', "ejs");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', (req,res)=>{
    res.render("home.ejs");
    console.log("Enter home page!");
    res.end();
});

app.get('/labby', (req,res)=>{
    res.render("labby.ejs");
});

app.get('/about', (req,res)=>{
    res.render("about.ejs");
});

app.listen(port, ()=>{
    console.log("Server open!");
});
